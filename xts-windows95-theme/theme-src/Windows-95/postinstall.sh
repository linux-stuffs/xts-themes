#!/bin/bash
# Postinstall will run after installation of this theme

killall conky
killall plank

xfconf-query -c xsettings -p /Gtk/CursorThemeName -n -t "string" -s "Chicago95_Cursor_Black"

xfce4-panel -r