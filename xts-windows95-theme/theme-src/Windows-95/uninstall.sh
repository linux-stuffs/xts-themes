#!/bin/bash
# Uninstall is for clean up settings of this theme
# before change to the new theme

xfconf-query -c xsettings -p /Gtk/CursorThemeName -n -t "string" -s ""

rm -rf ~/.config/xfce4/terminal/terminalrc
rm -rf ~/.config/gtk-3.0/gtk.css
