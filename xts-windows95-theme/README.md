# MS Windows 95 (Chicago) for Xfce4 Themes Switcher

This themes require [Xfce4](https://www.xfce.org/), [Xfce4 Theme Switcher](https://gitlab.com/linux-stuffs/xfce4-theme-switcher), [Chicago95](https://github.com/grassmunk/Chicago95), gtk-engines).

![Windows95](img-git/Windows95.png)

## INSTALLATION

### From the [AUR](https://aur.archlinux.org/packages/xts-widows95-theme/):
```
git clone https://aur.archlinux.org/chicago95-git.git
cd chicago95-git/
makepkg -sci

git clone https://aur.archlinux.org/xts-widows95-theme.git
cd xts-widows95-theme/
makepkg -sci
```

### Install from source:


Unpack the source package and run command (like root):
```
make install
```

### Uninstall from source:
Run command (like root):
```
make uninstall
```

### Build own ArchLinux package

You need these packages for building the ArchLinux package:

```
base-devel git wget yajl xfce4-theme-switcher xfce4-whiskermenu-plugin
chicago95-gtk-theme-git chicago95-icon-theme-git xcursor-chicago95-git
```

Run command:
```
make build-arch
```

### Install from *.pkg.tar.xz package:

Run command (like root):

```
pacman -U distrib/xts-widows95-theme*.pkg.tar.xz
```
