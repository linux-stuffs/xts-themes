# Themes for Xfce4 Theme Switcher

Prepared themes for [Xfce4 Themes Switcher](https://gitlab.com/linux-stuffs/xfce4-theme-switcher).

## Windows 10 Themes

![xts-windows10-theme](img-git/xts-windows10-theme.png)

Windows 10 Theme can be downloaded from [here](xts-windows10-theme/).

## Windows XP Theme

![xts-windowsxp-theme](img-git/xts-windowsxp-theme.png)

Windows Xp Theme can be downloaded from [here](xts-windowsxp-theme/).

## Windows Server 2003 Theme

![xts-server2003-theme](img-git/xts-server2003-theme.png)

Windows Server 2003 Theme can be downloaded from [here](xts-windows-server-2003-theme/).

## Windows 95 (Chicago) Theme

![xts-windows95-theme](img-git/xts-windows95-theme.png)

Windows 95 (Chicago) Theme can be downloaded from [here](xts-windows95-theme/).

## macOS Big Sur Theme

![xts-macos-big-sur-theme](img-git/xts-macos-big-sur-theme.png)

This theme can be downloaded from [here](xts-macos-big-sur-theme/).

## macOS Sierra Theme

![xts-macos-sierra-theme](img-git/xts-macos-sierra-theme.png)

This theme can be downloaded from [here](xts-macos-sierra-theme/).

## OS X Yosemite Theme

![xts-macos-yosemite-theme](img-git/xts-macos-yosemite-theme.png)

This theme can be downloaded from [here](xts-macos-yosemite-theme/).

## Dark Theme

![xts-dark-theme](img-git/xts-dark-theme.png)

Dark Theme can be downloaded from [here](xts-dark-theme/).

## ArcoLinux Theme

![xts-arcolinux-theme](img-git/xts-arcolinux-theme.png)

ArcoLinux Theme can be downloaded from [here](xts-arcolinux-theme/).
