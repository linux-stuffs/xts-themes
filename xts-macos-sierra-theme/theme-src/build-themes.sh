#!/bin/bash
# Name: build-themes
# Short-Description: bash script for build themes
# Version: 0.1.7

/usr/share/xfce4-theme-switcher/theme-switcher.sh -sp $(pwd)/macOS-Sierra-Dark/ macOS-Sierra-Dark
/usr/share/xfce4-theme-switcher/theme-switcher.sh -sp $(pwd)/macOS-Sierra-Light/ macOS-Sierra-Light
mv *.tar.gz ../contents/xfce4-theme-switcher/themes/

