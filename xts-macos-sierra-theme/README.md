# macOS Sierra for Xfce4 Themes Switcher

Two beautiful macOS Sierra themes for [Xfce4 Themes Switcher](https://gitlab.com/linux-stuffs/xfce4-theme-switcher).

This themes require [Xfce4](https://www.xfce.org/), [Xfce4 Theme Switcher](https://gitlab.com/linux-stuffs/xfce4-theme-switcher), [Plank](https://launchpad.net/plank), gtk-engine-murrine, gtk-engines, [sierra-gtk-theme-git](https://github.com/vinceliuice/Sierra-gtk-theme), [whitesur-icon-theme](https://github.com/vinceliuice/WhiteSur-icon-theme), [capitaine-cursors](https://github.com/keeferrourke/capitaine-cursors), [xfce4-docklike-plugin](https://gitlab.xfce.org/panel-plugins/xfce4-docklike-plugin), [vala-panel-appmenu-xfce](https://gitlab.com/vala-panel-project/vala-panel-appmenu), [rofi](https://github.com/davatorium/rofi).

This theme contains **Launchpad** and **Launcher** which are created in Rofi.

## Shortcuts:

Launcher - `<left CTRL>+Shift`
Launchpad - `<Super>+F4` or `<Win>+F4`

## Preview

### macOS Sierra Dark

![macOS-Sierra-Dark](img-git/macOS-Sierra-Dark.png)

### macOS Sierra Light

![macOS-Sierra-Light](img-git/macOS-Sierra-Light.png)


## INSTALLATION

### From the [AUR](https://aur.archlinux.org/packages/xts-macos-sierra-theme/):

```
git clone https://aur.archlinux.org/xts-macos-sierra-theme.git
cd xts-macos-big-sur-theme/
makepkg -sci
```

### Install from source:

Unpack the [source package](https://gitlab.com/linux-stuffs/xts-themes/-/tree/master/xts-macos-sierra-theme) and run command (like root):

```
make install
```

### Uninstall from source:

Run command (like root):

```
make uninstall
```

### Build own ArchLinux package

You need these packages for building the ArchLinux package:

```
yay -Sy base-devel git wget yajl sierra-gtk-theme-git whitesur-icon-theme capitaine-cursors xfce4-docklike-plugin vala-panel-appmenu-common vala-panel-appmenu-registrar vala-panel-appmenu-xfce appmenu-gtk-module rofi

ln -s /usr/share/themes/Sierra-light/plank /usr/share/plank/themes/Sierra-light
ln -s /usr/share/themes/Sierra-dark/plank /usr/share/plank/themes/Sierra-dark
```

Run command:

```
make build-arch
```

### Install from *.pkg.tar.xz package:

Run command (like root):

```
pacman -U distrib/xts-macos-sierra-theme*.pkg.tar.xz
```
