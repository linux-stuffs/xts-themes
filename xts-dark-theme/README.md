# Dark Theme for Xfce4 Themes Switcher

Nice own Dark theme for [Xfce4 Themes Switcher](https://gitlab.com/linux-stuffs/xfce4-theme-switcher).

This theme require Xfce4, Xfce4 Theme Switcher, Arc theme, Plank, Conky, Sardi icons, Whisker menu.and rofi.

This theme contains **Launchpad** and **Launcher** which are created in Rofi.

**Shortcuts:**

Launcher - `<left CTRL>+Shift`
Launchpad - `<Super>+F4` or `<Win>+F4`


![Dark-theme](img-git/Dark-theme.png)

## INSTALLATION

### From the [AUR](https://aur.archlinux.org/packages/xts-dark-theme/):
```
git clone https://aur.archlinux.org/xts-dark-theme.git
cd xts-dark-theme/
makepkg -sci
```

### Install from source:


Unpack the source package and run command (like root):
```
make install
```

### Uninstall from source:
Run command (like root):
```
make uninstall
```

### Build own ArchLinux package

For install Sardi icons from AUR run these commands (like normal user):

```
git clone https://aur.archlinux.org/sardi-icons.git
cd sardi-icons
makepkg -sci
cd ..
rm -rf sardi-icons
```

You need these packages for building the ArchLinux package:

```
base-devel git wget yajl sardi-icons xfce4-theme-switcher
xfce4-whiskermenu-plugin arc-gtk-theme gtk-engines plank conky
```

Run command:
```
make build-arch
```

### Install from *.pkg.tar.xz package:

Run command (like root):

```
pacman -U distrib/xts-dark-theme*.pkg.tar.xz
```
