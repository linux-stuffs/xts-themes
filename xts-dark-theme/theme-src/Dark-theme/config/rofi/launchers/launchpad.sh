#!/usr/bin/env bash
#
## Author : Pavel Sibal
## Rofi   : Launchpad
#

theme='Launchpad-02.rasi'
icon_theme='Sardi'

## Run
rofi \
     -show drun \
     -icon-theme ${icon_theme} \
     -font 'San Regular 10' \
     -theme ${theme}
