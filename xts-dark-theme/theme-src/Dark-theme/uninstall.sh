#!/bin/bash
# Uninstall is for clean up settings of this theme 
# before change to the new theme

killall plank
killall conky

xfconf-query -c xfce4-keyboard-shortcuts -p '/commands/custom/<Primary>space' -r
xfconf-query -c xfce4-keyboard-shortcuts -p '/commands/custom/<Super>F4' -r
xfconf-query -c xfwm4 -p /general/inactive_opacity -n -t int -s 100
xfconf-query -c xfwm4 -p /general/show_dock_shadow -n -t bool -s true
xfconf-query -c xfwm4 -p /general/show_frame_shadow -n -t bool -s true
xfconf-query -c xfwm4 -p /general/show_popup_shadow -n -t bool -s true
xfconf-query -c thunar -p /misc-middle-click-in-tab -r
xfconf-query -c thunar -p /misc-single-click -r
rm -rf ~/.config/autostart/Conky-session.desktop
rm -rf ~/.config/autostart/Plank.desktop
rm -rf ~/.config/conky/conky-sessionfile
rm -rf ~/.config/conky/GothamClockBrown.conkyrc
rm -rf ~/.config/conky/TTProcessPanelBrown.conkyrc
rm -rf ~/.config/gtk-3.0/gtk.css
rm -rf ~/.config/gtk-3.0/whisker-dark.css
rm -rf ~/.config/gtk-3.0/xfce4-session-logout.css
rm -rf ~/.config/plank
rm -rf ~/.config/rofi/launchers/findapp.sh
rm -rf ~/.config/rofi/launchers/launchpad.sh
rm -rf ~/.config/xfce4/terminal/terminalrc
rm -rf ~/.local/share/applications/Launchpad.desktop
rm -rf ~/.local/share/fonts/GE_Inspira.ttf
rm -rf ~/.local/share/fonts/Ubuntu.ttf
rm -rf ~/.local/share/icons/rofi-launchpad.svg
rm -rf ~/.local/share/rofi/themes/FindApp-01.rasi
rm -rf ~/.local/share/rofi/themes/Launchpad-02.rasi
