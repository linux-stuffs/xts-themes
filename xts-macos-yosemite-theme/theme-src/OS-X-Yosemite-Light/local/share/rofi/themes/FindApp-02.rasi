/**
 *
 * Author : Pavel Sibal
 *
 * FindApp 02
 *
 * Rofi Version: 1.7.3
 *
 **/

/***********************************/
/*****----- Configuration -----*****/
/***********************************/
configuration {
    no-lazy-grab:               true;
    drun-display-format:        "{name}";
    modi:                       "drun,filebrowser,window";
    show-icons:                 true;
    display-drun:               "";
    /*display-run:                "";*/
    display-filebrowser:        "";
    display-window:             "";
    drun-display-format:        "{name}";
    window-format:              "{w} · {c} · {t}";
    disable-history:            true;
    me-select-entry:            "MouseSecondary";
    me-accept-entry:            "MousePrimary";
}

/***************************************/
/*****----- Global Properties -----*****/
/***************************************/

/*****----- Solid Light Background -----*****/
* {
   background:     #F5F5F5E5;
   background-alt: #D0D0D0E5;
   foreground:     #000000E5;
   selected:       #EBEBEBE5;
   border:         #FFFFFFE5;
   border-radius:  6px; /* 0px normal corners */
}

/*****----- Solid Gray Background -----*****/
/* for enable remove
* {
   background:     #2A2A2AFF;
   background-alt: #222222FF;
   foreground:     #FFFFFFFF;
   selected:       #FFFFFF26;
   border:         #FFFFFFFF;
   border-radius:  6px; /* 0px normal corners */
}
for enable remove here */

/*******************************/
/*****----- Rofi code -----*****/
/*******************************/

/*****----- Main Window -----*****/
window {
    transparency:                "real";
    location:                    center;
    anchor:                      center;
    fullscreen:                  false;
    width:                       420px;
    x-offset:                    0px;
    y-offset:                    0px;
    enabled:                     true;
    margin:                      0px;
    padding:                     0px;
    border:                      0px solid;
    border-radius:               @border-radius;
    border-color:                @border;
    background-color:            @background;
    cursor:                      "default";
}

/*****----- Main Box -----*****/
mainbox {
    enabled:                     true;
    spacing:                     0px;
    margin:                      0px;
    padding:                     0px;
    border:                      0px solid;
    border-radius:               0px;
    border-color:                @border;
    background-color:            transparent;
    children:                    [ "inputbar", "listbox" ];
}

listbox {
    spacing:                     10px;
    padding:                     0px;
    border-radius:               0px;
    background-color:            transparent;
    orientation:                 vertical;
    children:                    [ "message", "listview" ];
}

/*****----- Inputbar -----*****/
inputbar {
    enabled:                     true;
    spacing:                     10px;
    margin:                      0px;
    padding:                     14px;
    border:                      0px solid;
    border-radius:               0px;
    border-color:                @border;
    background-color:            @background-alt;
    text-color:                  @foreground;
    children:                    [ "entry", "mode-switcher" ];
}

prompt {
    enabled:                     true;
    background-color:            inherit;
    text-color:                  inherit;
}

entry {
    enabled:                     true;
    expand:                      true;
    background-color:            inherit;
    border-radius:               0px;
    text-color:                  inherit;
    cursor:                      text;
    placeholder:                 "Search...";
    placeholder-color:           inherit;
    margin:                      4px 0px 0px 0px;
    padding:                     0px;
}

/*****----- Mode Switcher -----*****/
mode-switcher{
    enabled:                     true;
    spacing:                     5px;
    background-color:            transparent;
    text-color:                  @foreground;
}

button {
    width:                       30px;
    padding:                     5px;
    border-radius:               100%;
    border:                      1px solid;
    border-color:                @selected;
    background-color:            @background-alt;
    text-color:                  inherit;
    cursor:                      pointer;
}

button selected {
    background-color:            @selected;
    text-color:                  @foreground;
}

/*****----- Listview -----*****/
listview {
    enabled:                     true;
    columns:                     1;
    lines:                       7;
    cycle:                       true;
    dynamic:                     true;
    scrollbar:                   false;
    layout:                      vertical;
    reverse:                     false;
    fixed-height:                true;
    fixed-columns:               true;
    spacing:                     0px;
    margin:                      0px;
    padding:                     0px;
    border:                      0px solid;
    border-radius:               0px;
    border-color:                @border;
    background-color:            @background;
    text-color:                  @foreground;
    cursor:                      "default";
}

scrollbar {
    handle-width:                5px ;
    handle-color:                @selected;
    border-radius:               0px;
    background-color:            @background-alt;
}

/*****----- Elements -----*****/
element {
    enabled:                     true;
    spacing:                     10px;
    margin:                      0px;
    padding:                     8px 14px;
    border:                      0px solid;
    border-radius:               0px;
    border-color:                @border;
    background-color:            @background;
    text-color:                  @foreground;
    cursor:                      pointer;
}

element normal.normal {
    background-color:            @background;
    text-color:                  @foreground;
}

element selected.normal {
    background-color:            @selected;
    text-color:                  @foreground;
}

element-icon {
    background-color:            transparent;
    text-color:                  inherit;
    size:                        32px;
    cursor:                      inherit;
}

element-text {
    background-color:            transparent;
    text-color:                  inherit;
    highlight:                   inherit;
    cursor:                      inherit;
    vertical-align:              0.5;
    horizontal-align:            0.0;
}

/*****----- Message -----*****/
error-message {
    padding:                     15px;
    border:                      2px solid;
    border-radius:               12px;
    border-color:                @border;
    background-color:            @background;
    text-color:                  @foreground;
}
textbox {
    background-color:            @background;
    text-color:                  @foreground;
    vertical-align:              0.5;
    horizontal-align:            0.0;
    highlight:                   none;
}

