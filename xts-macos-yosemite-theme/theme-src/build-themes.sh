#!/bin/bash
# Name: build-themes
# Short-Description: bash script for build themes
# Version: 0.1.7

/usr/share/xfce4-theme-switcher/theme-switcher.sh -sp $(pwd)/OS-X-Yosemite-Dark/ OS-X-Yosemite-Dark
/usr/share/xfce4-theme-switcher/theme-switcher.sh -sp $(pwd)/OS-X-Yosemite-Light/ OS-X-Yosemite-Light
mv *.tar.gz ../contents/xfce4-theme-switcher/themes/
