/configver 2
/panels [<1>]
/panels/dark-mode false
/panels/panel-1/background-rgba [<0.16470588235294117>, <0.16470588235294117>, <0.16470588235294117>, <0.44680851063829785>]
/panels/panel-1/background-style uint32 0
/panels/panel-1/enter-opacity uint32 100
/panels/panel-1/icon-size uint32 16
/panels/panel-1/leave-opacity uint32 100
/panels/panel-1/length uint32 100
/panels/panel-1/plugin-ids [<9>, <1>, <26>, <7>, <3>, <17>, <18>, <4>, <5>, <24>, <25>, <6>, <8>, <10>, <11>, <12>, <13>, <14>, <19>]
/panels/panel-1/position 'p=6;x=0;y=0'
/panels/panel-1/position-locked true
/panels/panel-1/size uint32 30
/plugins/plugin-1 'applicationsmenu'
/plugins/plugin-1/button-icon 'start-here'
/plugins/plugin-1/button-title 'Applications'
/plugins/plugin-1/custom-menu true
/plugins/plugin-1/custom-menu-file '.config/menu/xpple.menu'
/plugins/plugin-1/show-button-title false
/plugins/plugin-10 'notification-plugin'
/plugins/plugin-11 'separator'
/plugins/plugin-11/style uint32 0
/plugins/plugin-12 'clock'
/plugins/plugin-13 'separator'
/plugins/plugin-13/style uint32 0
/plugins/plugin-14 'actions'
/plugins/plugin-14/appearance uint32 0
/plugins/plugin-14/items [<'-lock-screen'>, <'-switch-user'>, <'-separator'>, <'-suspend'>, <'-hibernate'>, <'-hybrid-sleep'>, <'-separator'>, <'-shutdown'>, <'-restart'>, <'-separator'>, <'+logout'>, <'-logout-dialog'>]
/plugins/plugin-17 'docklike'
/plugins/plugin-18 'separator'
/plugins/plugin-18/expand true
/plugins/plugin-18/style uint32 0
/plugins/plugin-19 'separator'
/plugins/plugin-19/style uint32 0
/plugins/plugin-24 'weather'
/plugins/plugin-24/cache-max-age 172800
/plugins/plugin-24/forecast/days 5
/plugins/plugin-24/forecast/layout 1
/plugins/plugin-24/msl 189
/plugins/plugin-24/offset '+01:00'
/plugins/plugin-24/power-saving true
/plugins/plugin-24/round true
/plugins/plugin-24/scrollbox/animate true
/plugins/plugin-24/scrollbox/color 'rgba(0,0,0,0)'
/plugins/plugin-24/scrollbox/lines 1
/plugins/plugin-24/scrollbox/show true
/plugins/plugin-24/scrollbox/use-color false
/plugins/plugin-24/single-row true
/plugins/plugin-24/theme-dir '/usr/share/xfce4/weather/icons/liquid'
/plugins/plugin-24/timezone ''
/plugins/plugin-24/tooltip-style 1
/plugins/plugin-24/units/altitude 0
/plugins/plugin-24/units/apparent-temperature 0
/plugins/plugin-24/units/precipitation 0
/plugins/plugin-24/units/pressure 0
/plugins/plugin-24/units/temperature 0
/plugins/plugin-24/units/windspeed 0
/plugins/plugin-25 'xkb'
/plugins/plugin-25/display-scale uint32 50
/plugins/plugin-25/display-type uint32 1
/plugins/plugin-25/group-policy uint32 0
/plugins/plugin-26 'separator'
/plugins/plugin-26/expand false
/plugins/plugin-26/style uint32 0
/plugins/plugin-3 'separator'
/plugins/plugin-3/expand true
/plugins/plugin-3/style uint32 0
/plugins/plugin-4 'pager'
/plugins/plugin-4/rows uint32 1
/plugins/plugin-4/wrap-workspaces false
/plugins/plugin-5 'separator'
/plugins/plugin-5/style uint32 0
/plugins/plugin-6 'systray'
/plugins/plugin-6/icon-size 22
/plugins/plugin-6/known-legacy-items [<'applet networkmanager'>, <'pamac-tray'>]
/plugins/plugin-6/square-icons true
/plugins/plugin-7 'appmenu'
/plugins/plugin-7/plugins/plugin-7/bold-application-name false
/plugins/plugin-7/plugins/plugin-7/compact-mode false
/plugins/plugin-7/plugins/plugin-7/expand false
/plugins/plugin-8 'pulseaudio'
/plugins/plugin-8/enable-keyboard-shortcuts true
/plugins/plugin-8/show-notifications true
/plugins/plugin-9 'separator'
/plugins/plugin-9/style uint32 0