#!/bin/bash
# Postinstall will run after installation of this theme

killall conky
killall plank

xfconf-query -c xsettings -p /Gtk/ShellShowsMenubar -n -t bool -s true
xfconf-query -c xsettings -p /Gtk/ShellShowsAppmenu -n -t bool -s true
xfconf-query -c thunar -p /misc-single-click -n -t bool -s true
xfconf-query -c xfwm4 -p /general/inactive_opacity -n -t int -s 90
xfconf-query -c xfwm4 -p /general/show_dock_shadow -n -t bool -s false
xfconf-query -c xfwm4 -p /general/show_frame_shadow -n -t bool -s false
xfconf-query -c xfwm4 -p /general/show_popup_shadow -n -t bool -s false
xfconf-query -c thunar -p /misc-middle-click-in-tab -n -t bool -s true
xfconf-query -c xfce4-keyboard-shortcuts -n -t 'string' -p '/commands/custom/<Primary>space' -s "sh -c '~/.config/rofi/launchers/findapp.sh'"
xfconf-query -c xfce4-keyboard-shortcuts -n -t 'string' -p '/commands/custom/<Super>F4' -s "sh -c '~/.config/rofi/launchers/launchpad.sh'"
xfconf-query -c xfce4-panel -p /plugins/plugin-1/custom-menu-file -n -t 'string' -s ${HOME}'/.config/menu/xpple.menu'

gtk-update-icon-cache $HOME/.local/share/icons/WhiteSur/
sed -i "s|HOMEDIR|$HOME|g" ~/.config/plank/dock1/launchers/Launchpad.dockitem

xfce4-panel -r
nohup plank > /dev/null 2>&1&

