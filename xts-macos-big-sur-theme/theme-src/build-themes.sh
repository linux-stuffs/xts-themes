#!/bin/bash
# Name: build-themes
# Short-Description: bash script for build themes
# Version: 0.1.7

/usr/share/xfce4-theme-switcher/theme-switcher.sh -sp $(pwd)/macOS-Big-Sur-Dark/ macOS-Big-Sur-Dark
/usr/share/xfce4-theme-switcher/theme-switcher.sh -sp $(pwd)/macOS-Big-Sur-Dark-Solid/ macOS-Big-Sur-Dark-Solid
/usr/share/xfce4-theme-switcher/theme-switcher.sh -sp $(pwd)/macOS-Big-Sur-Light/ macOS-Big-Sur-Light
/usr/share/xfce4-theme-switcher/theme-switcher.sh -sp $(pwd)/macOS-Big-Sur-Light-Solid/ macOS-Big-Sur-Light-Solid
mv *.tar.gz ../contents/xfce4-theme-switcher/themes/

