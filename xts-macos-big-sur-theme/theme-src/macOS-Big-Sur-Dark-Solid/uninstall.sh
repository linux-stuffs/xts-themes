#!/bin/bash
# Uninstall is for clean up settings of this theme
# before change to the new theme

killall conky
killall plank

xfconf-query -c xsettings -p /Gtk/ShellShowsMenubar -n -t bool -s false
xfconf-query -c xsettings -p /Gtk/ShellShowsAppmenu -n -t bool -s false
xfconf-query -c xfce4-keyboard-shortcuts -p '/commands/custom/<Primary>space' -r
xfconf-query -c xfce4-keyboard-shortcuts -p '/commands/custom/<Super>F4' -r
xfconf-query -c xfce4-panel -p /plugins/plugin-1/custom-menu-file -n -t 'string' -s ''
xfconf-query -c xfce4-panel -p /plugins/plugin-1/custom-menu -n -t bool -s false
xfconf-query -c xfwm4 -p /general/inactive_opacity -n -t int -s 100
xfconf-query -c xfwm4 -p /general/show_dock_shadow -n -t bool -s true
xfconf-query -c xfwm4 -p /general/show_frame_shadow -n -t bool -s true
xfconf-query -c xfwm4 -p /general/show_popup_shadow -n -t bool -s true
xfconf-query -c thunar -p /misc-middle-click-in-tab -r
xfconf-query -c thunar -p /misc-single-click -r

rm -rf ~/.config/autostart/Plank.desktop
rm -rf ~/.config/menu/xpple.menu
rm -rf ~/.config/plank
rm -rf ~/.config/rofi/launchers/findapp.sh
rm -rf ~/.config/rofi/launchers/launchpad.sh
rm -rf ~/.config/xfce4/terminal/terminalrc
rm -rf ~/.local/share/applications/Launchpad.desktop
rm -rf ~/.local/share/applications/pamac.manager.desktop
rm -rf ~/.local/share/applications/xfce-settings-manager.desktop
rm -rf ~/.local/share/applications/xfce4-about.desktop
rm -rf ~/.local/share/applications/xfce4-session-lockscreen.desktop
rm -rf ~/.local/share/applications/xfce4-session-logout.desktop
rm -rf ~/.local/share/fonts/GrapeNuts-Regular.ttf
rm -rf ~/.local/share/fonts/Icomoon-Feather.ttf
rm -rf ~/.local/share/fonts/Iosevka-Nerd-Font-Complete.ttf
rm -rf ~/.local/share/fonts/iosevka-term
rm -rf ~/.local/share/fonts/JetBrains-Mono-Nerd-Font-Complete.ttf
rm -rf ~/.local/share/fonts/Radio_Space.ttf
rm -rf ~/.local/share/fonts/SanFranciscoFont-master
rm -rf ~/.local/share/icons/ditoMenu-spotlight.svg
rm -rf ~/.local/share/icons/ditoMenu-spotlight-white.svg
rm -rf ~/.local/share/icons/rofi-launchpad.svg
rm -rf ~/.local/share/icons/sideBar-controlCentre.svg
rm -rf ~/.local/share/icons/sideBar-controlCentre-white.svg
rm -rf ~/.config/plank/dock1/launchers/desktop.dockitem
rm -rf ~/.config/plank/dock1/launchers/Launchpad.dockitem
rm -rf ~/.config/plank/dock1/launchers/org.manjaro.pamac.manager.dockitem
rm -rf ~/.config/plank/dock1/launchers/xfce4-file-manager.dockitem
rm -rf ~/.config/plank/dock1/launchers/xfce4-terminal-emulator.dockitem
rm -rf ~/.config/plank/dock1/launchers/xfce4-web-browser.dockitem
rm -rf ~/.local/share/rofi/themes/FindApp-01.rasi
rm -rf ~/.local/share/rofi/themes/Launchpad-02.rasi
