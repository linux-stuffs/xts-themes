#!/usr/bin/env bash
#
## Author : Pavel Sibal
## Rofi   : Launchpad
#

theme='Launchpad-02.rasi'
icon_theme='WhiteSur-dark'

## Run
rofi \
     -show drun \
     -icon-theme ${icon_theme} \
     -font 'San Francisco Display Regular 10' \
     -theme ${theme}
