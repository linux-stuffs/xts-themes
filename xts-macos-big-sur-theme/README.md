# macOS Big Sur Themes for Xfce4 Themes Switcher

Two beautiful macOS Big Sur themes for [Xfce4 Themes Switcher](https://gitlab.com/linux-stuffs/xfce4-theme-switcher).

This themes require [Xfce4](https://www.xfce.org/), [Xfce4 Theme Switcher](https://gitlab.com/linux-stuffs/xfce4-theme-switcher), [Plank](https://launchpad.net/plank), gtk-engine-murrine, gtk-engines, [whitesur-gtk-theme](https://github.com/vinceliuice/WhiteSur-gtk-theme), [whitesur-icon-theme](https://github.com/vinceliuice/WhiteSur-icon-theme), [whitesur-cursor-theme](https://github.com/vinceliuice/WhiteSur-cursors), [xfce4-docklike-plugin](https://gitlab.xfce.org/panel-plugins/xfce4-docklike-plugin), [vala-panel-appmenu-xfce](https://gitlab.com/vala-panel-project/vala-panel-appmenu), [rofi](https://github.com/davatorium/rofi).

This theme contains **Launchpad** and **Launcher** which are created in Rofi.

## Shortcuts:

Launcher - `<left CTRL>+Shift`
Launchpad - `<Super>+F4` or `<Win>+F4`

## Preview

### macOS Big Sur Dark

![macOS-Big-Sur-Dark](img-git/macOS-Big-Sur-Dark.png)

### macOS Big Sur Dark Solid

![macOS-Big-Sur-Dark-Solid](img-git/macOS-Big-Sur-Dark-Solid.png)

### macOS Big Sur Light

![macOS-Big-Sur-Light](img-git/macOS-Big-Sur-Light.png)

### macOS Big Sur Light Solid

![macOS-Big-Sur-Light-Solid](img-git/macOS-Big-Sur-Light-Solid.png)

## INSTALLATION

### From the [AUR](https://aur.archlinux.org/packages/xts-macos-big-sur-theme/):

```
git clone https://aur.archlinux.org/xts-macos-big-sur-theme.git
cd xts-macos-big-sur-theme/
makepkg -sci
```

### Install from source:

Unpack the [source package](https://gitlab.com/linux-stuffs/xts-themes/-/tree/master/xts-macos-big-sur-theme) and run command (like root):

```
make install
```

### Uninstall from source:

Run command (like root):

```
make uninstall
```

### Build own ArchLinux package

You need these packages for building the ArchLinux package:

```
yay -Sy base-devel git wget yajl whitesur-gtk-theme whitesur-icon-theme whitesur-cursor-theme-git xfce4-docklike-plugin vala-panel-appmenu-common vala-panel-appmenu-registrar vala-panel-appmenu-xfce appmenu-gtk-module rofi

ln -s /usr/share/themes/WhiteSur-Dark/plank /usr/share/plank/themes/WhiteSur-Dark
ln -s /usr/share/themes/WhiteSur-Dark-solid/plank /usr/share/plank/themes/WhiteSur-Dark-solid
ln -s /usr/share/themes/WhiteSur-Light/plank /usr/share/plank/themes/WhiteSur-Light
ln -s /usr/share/themes/WhiteSur-Light-solid/plank /usr/share/plank/themes/WhiteSur-Light-solid
```

Run command:

```
make build-arch
```

### Install from *.pkg.tar.xz package:

Run command (like root):

```
pacman -U distrib/xts-macos-big-sur-theme*.pkg.tar.xz
```
